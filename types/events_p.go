package types

import tmjson "github.com/tendermint/tendermint/libs/json"

const (
	EventPendingTx            = "PendingTx"
	EventPendingTxWithReceipt = "PendingTxWithReceipt"
)

func init() {
	tmjson.RegisterType(EventDataPendingTx{}, "tendermint/event/PendingTx")
	tmjson.RegisterType(EventDataPendingTxWithReceipt{}, "tendermint/event/EventDataPendingTxWithReceipt")
}

type EventDataPendingTx struct {
	Tx []byte `json:"tx"`
}

type EventDataPendingTxWithReceipt struct {
	Tx      []byte `json:"tx"`
	Receipt []byte `json:"receipt"`
}
